Feature: UI tests for playground page
  Background:
    Given Open Home Page

  Scenario: Smoke tests for the links in home page
    Then  Every link should open correct page

  Scenario:  Set New Button Name
    When User open TestInput page
    And  User type word in input and click
    Then Button name should change

  Scenario:  Progress bar functionality check
    When User open ProgressBar page
    And  User clicks Start button
    And  User wait %75 filled in Progress Bar to click stop button
    Then Result should be zero