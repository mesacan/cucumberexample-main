import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Mehmet Engin Sacan
 * This class contains home page related variables and methods.
 */
public class HomePage extends BaseTest{

    private String textInputId = "newButtonName";
    private String buttonId = "updatingButton";
    private String testText = "TestingForSpriteCloud";
    private String pageLinksXpath = "//h3//a";
    List<WebElement> pageNames = findElements(By.xpath(pageLinksXpath));
    String url="";
    /**
     * Method is checking all html element "a" for links and gets the empty links
     * If there is any empty link method will print errors and returns false
     *
     * @return true
     */
    public boolean checkAllPageLinks() {

        Iterator<WebElement> pageIterator = pageNames.iterator();
        List<WebElement> emptyLinks = new ArrayList<>();
        // Reaching Each link element and collecting empty links
        while(pageIterator.hasNext()) {
            url = pageIterator.next().getAttribute("href");
            if (url == null || url.isEmpty()) {
                emptyLinks.add(pageIterator.next());
                continue;
            }
        }

        if (emptyLinks.isEmpty()) {
            return true;
        } else {
            for (WebElement emptyLink : emptyLinks) {
                logger.error("Empty Links: "+emptyLink.getText());
            }
            return false;
        }
    }
}
