package api;

import api.responses.OrderResponse;
import api.responses.PetResponse;
import api.responses.UserResponse;
import com.google.gson.Gson;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;


/**
 * @author Mehmet Engin Sacan
 * This class is to create pet related api calls and payloads as factory
 *
 */
public class PetFactory {
    static HttpClient httpClient =
            HttpClient.newBuilder()
                    .version(HttpClient.Version.HTTP_1_1)
                    .connectTimeout(Duration.ofSeconds(20))
                    .followRedirects(HttpClient.Redirect.NORMAL)
                    .build();

    static Gson gson = new Gson();
    static Yaml yml = new Yaml();

    public static HttpResponse<String> post(String path, Map data)
            throws IOException, InterruptedException {
        HttpRequest request;
        HttpResponse<String> response;
        request =
                HttpRequest.newBuilder()
                        .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(data)))
                        .uri(
                                URI.create(
                                        "https://petstore.swagger.io/v2/" + path))
                        .setHeader("Content-Type", "application/json")
                        .setHeader("Accept", " application/json")
                        .build();
        response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response;
    }

    public static HttpResponse<String> get(String path, String parameterData)
            throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request;
        HttpResponse<String> response;
        request =
                HttpRequest.newBuilder()
                        .GET()
                        .uri( new URI("https://petstore.swagger.io/v2/" + path + "/"+ parameterData))
                        .setHeader("Accept", " application/json")
                        .build();
        response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response;
    }
    public static HttpResponse<String> delete(String path, String parameterData)
            throws IOException, InterruptedException, URISyntaxException {
        HttpRequest request;
        HttpResponse<String> response;
        request =
                HttpRequest.newBuilder()
                        .DELETE()
                        .uri( new URI("https://petstore.swagger.io/v2/"+ path + "/"+ parameterData))
                        .setHeader("Accept", " application/json")
                        .build();
        response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response;
    }
    // Returning necessary tag for assert functions

    public static int returnPetId (HttpResponse<String> response) {
        PetResponse petResponse = gson.fromJson(response.body(), PetResponse.class);
        return petResponse.getId();
    }

    public static int returnOrderId (HttpResponse<String> response) {
        OrderResponse orderResponse = gson.fromJson(response.body(), OrderResponse.class);
        return orderResponse.getId();
    }
    public static int returnOrderPetId (HttpResponse<String> response) {
        OrderResponse orderResponse = gson.fromJson(response.body(), OrderResponse.class);
        return orderResponse.getPetId();
    }

    public static int returnUserCode (HttpResponse<String> response) {
        UserResponse userResponse = gson.fromJson(response.body(), UserResponse.class);
        return userResponse.getCode();
    }

    // Creating data for payload
    public static Map createPet(int id, String name) throws IOException {
        String path =
                "./src/test/resources/petpayloads/add-pet.yml";
        var pet = yml.loadAs(Files.readString(Path.of(path)), Map.class);
        pet.put("id", id);
        pet.put("name", name);
        return pet;
    }
    public static Map orderPet(int id, int petId) throws IOException {
        DateFormat df = new SimpleDateFormat(("yyyy-MM-dd'T'HH:mm:ss'Z'"));
        var shipDate = Date.from(Instant.now());
        String path = "./src/test/resources/petpayloads/order-pet.yml";
        var order = yml.loadAs(Files.readString(Path.of(path)), Map.class);
        order.put("id", id);
        order.put("petId", petId);
        order.put("shipDate", df.format(shipDate));
        return order;
    }

    public static Map createUser(int id, String userName, String firstName, String lastName, String email,String password, String phone, int userStatus) throws IOException {
        String path = "./src/test/resources/petpayloads/create-user.yml";
        var user = yml.loadAs(Files.readString(Path.of(path)), Map.class);
        user.put("id", id);
        user.put("username", userName);
        user.put("firstName", firstName);
        user.put("lastName", lastName);
        user.put("email", email);
        user.put("password", password);
        user.put("phone", phone);
        user.put("userStatus", userStatus);
        return user;
    }
}
