package api;



import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Mehmet Engin Sacan
 * This steps definitions are related with pet store api service
 */
public class ApiTestSteps {

    private HttpResponse<String> result;

    @When("User wants to add a pet in the system")
    public void userWantsToAddAPetInTheSystem() throws IOException, InterruptedException {
        var pet = PetFactory.createPet(15, "testpet15");
        result = PetFactory.post("pet", pet);
    }

    @Then("User receive an id for pet")
    public void userReceiveAnIdForPet() {
        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
        assertThat(PetFactory.returnPetId(result)).isEqualTo(15);
    }

    @Given("Pet defined to system")
    public void petDefinedToSystem() throws IOException, InterruptedException {
        var pet = PetFactory.createPet(20, "testpet20");
        result = PetFactory.post("pet", pet);
        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
        assertThat(PetFactory.returnPetId(result)).isEqualTo(20);
    }

    @When("User wants to create an order for pet")
    public void userWantsToCreateAnOrderForPet() throws IOException, InterruptedException {
        var order = PetFactory.orderPet(1, 20);
        result = PetFactory.post("store/order", order);
    }

    @Then("Order should be contain necessary info")
    public void orderShouldBeContainNecessaryInfo() throws IOException, URISyntaxException, InterruptedException {

        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
        assertThat(PetFactory.returnOrderId(result)).isEqualTo(1);
        assertThat(PetFactory.returnOrderPetId(result)).isEqualTo(20);

        // This data should be checked also with get method.
        result = PetFactory.get("store/order","1");
        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
        assertThat(PetFactory.returnOrderId(result)).isEqualTo(1);
        assertThat(PetFactory.returnOrderPetId(result)).isEqualTo(20);

    }

    @Given("User created for pet store")
    public void userCreatedForPetStore() throws IOException, InterruptedException {
        var user = PetFactory.createUser(4, "testMes","Mes","test","testMes@test.com","testMes","12345678",1);
        result = PetFactory.post("user", user);
        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
        assertThat(PetFactory.returnUserCode(result)).isEqualTo(200);
    }
    @When("User wanted to delete in the system")
    public void userWantedToDeleteInTheSystem() throws IOException, URISyntaxException, InterruptedException {
        result = PetFactory.delete("user", "testMes");

    }
    @Then("System should add this user to the system")
    public void systemShouldAddThisUserToTheSystem() {
        assertThat(result.statusCode()).isIn(200, 201);
        assertThat(result.body()).isNotNull();
    }



}
