package api.responses;
/**
 * @author Mehmet Engin Sacan
 * This class is created for mapping response of the user creation
 */
public class UserResponse {

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    int code;
        String type;
        String message;

}
