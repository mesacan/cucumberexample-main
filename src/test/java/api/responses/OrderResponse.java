package api.responses;

import java.util.Date;

/**
 * @author Mehmet Engin Sacan
 * This class is created for mapping response of the order creation
 */
public class OrderResponse {

    public int getId() {
        return id;
    }

    public int getPetId() {
        return petId;
    }

    public int getQuantity() {
        return quantity;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public String getStatus() {
        return status;
    }

    public boolean isComplete() {
        return complete;
    }

    int id;
        int petId;
        int quantity;
        Date shipDate;
        String status;
        boolean complete;

}
