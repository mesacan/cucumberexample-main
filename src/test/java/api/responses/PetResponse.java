package api.responses;

import java.util.List;

/**
 * @author Mehmet Engin Sacan
 * This class is created for mapping response of the pet creation
 */
public class PetResponse {

    public int getId() {
        return id;
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public List<Category> getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    int id;
    Category category;
    String name;
    List<String> photoUrls;
    List<Category> tags;
    String status;

    public class Category {
        int id;
        String name;
    }


}
