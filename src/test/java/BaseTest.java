import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Mehmet Engin Sacan
 * Thıs class ıs the base class that everythıng related wıth UI tests
 *
 */
public class BaseTest {
    private int timeoutInSeconds = 10;
    protected static WebDriver driver;
    String baseUrl = "http://uitestingplayground.com/";
    String testText = "TestingForSpriteCloud";

    int result = -2;
    protected static Logger logger = Logger.getLogger(BaseTest.class);

    protected WebElement findElement(By by)  throws NotFoundException {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return driver.findElement(by);
    }
    protected List<WebElement> findElements(By by)  throws NotFoundException {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return driver.findElements(by);
    }

    protected void clickElement(WebElement element) throws NotFoundException {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }
    protected void hoverElement (WebElement element) {
        Actions action = new Actions(driver);
        action.moveToElement(element).perform();
    }
    protected void goToPage(String website) throws NotFoundException {
        driver.get(website);
    }
}
