import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

/**
 *  @author Mehmet Engin Sacan
 *  This class contains TextInputPage page related variables and methods.
 */
public class TextInputPage extends BaseTest {
    private String textInputId = "newButtonName";
    private String buttonId = "updatingButton";

    public boolean giveTextAndClick(String testText) {
        try {
            WebElement inputText = findElement(By.id(textInputId));
            inputText.sendKeys(testText);
            WebElement loginButton = findElement(By.id(buttonId));
            clickElement(loginButton);
        } catch ( NotFoundException e) {
            logger.error(e);
            return false;
        }
        return true;
    }
    public String getTextInButton() {
        try {
            WebElement loginButton = findElement(By.id(buttonId));
            return loginButton.getText();
        } catch ( NotFoundException e) {
            logger.error(e);
        }
        return null;
    }
}
