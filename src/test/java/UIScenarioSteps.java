import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

/**
 * @author Mehmet Engin Sacan
 * This class is the step definition class for UI tests.
 */
public class UIScenarioSteps extends BaseTest{

    @Given("Open Home Page")
    public void openHomePage() {
        goToPage(baseUrl);
        Assert.assertEquals(driver.getCurrentUrl(),baseUrl);
    }

    @Then("Every link should open correct page")
    public void everyLinkShouldWork() {
        HomePage homePage = new HomePage();
        Assert.assertTrue(homePage.checkAllPageLinks());
    }

    @When("^User open (.*) page$")
    public void userOpenPage(String page) {
        if (page.equals("TestInput")) {
            goToPage("http://www.uitestingplayground.com/textinput");
        }
        if (page.equals("ProgressBar")) {
            goToPage("http://www.uitestingplayground.com/progressbar");
        }
    }

    @When("User type word in input and click")
    public void userTypeWordInInputAndClick() {

        TextInputPage textInputPage = new TextInputPage();
        textInputPage.giveTextAndClick(testText);
    }

    @Then("Button name should change")
    public void buttonNameShouldChange() {
        TextInputPage textInputPage = new TextInputPage();
        Assert.assertEquals(textInputPage.getTextInButton(),testText);
    }

    @And("User clicks Start button")
    public void userClicksStartButton() {

        ProgressBarPage progressBarPage = new ProgressBarPage();
        progressBarPage.clickButton("Start");
    }

    @And("User wait %{int} filled in Progress Bar to click stop button")
    public void userWaitFilledInProgressBarToClickStopButton(int percent) {
        ProgressBarPage progressBarPage = new ProgressBarPage();
        result = progressBarPage.checkProgressBarAndClickStopButton(percent);
    }
    @Then("Result should be zero")
    public void resultShouldBeZero() {
        Assert.assertEquals(0,result);
    }
}
