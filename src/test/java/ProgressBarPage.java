import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

/**
 *  @author Mehmet Engin Sacan
 *  This class contains progressbar page related variables and methods.
 */
public class ProgressBarPage extends BaseTest{
    private String startButtonId ="startButton";
    private String stopButtonId ="stopButton";
    private String progressBarId = "progressBar";
    private String resultId = "result";
    public void clickButton(String btnType) {
        try {
            WebElement button = null;
            if (btnType.equals("Start")) {
                button = findElement(By.id(startButtonId));
            }
            if (btnType.equals("Stop")) {
                button = findElement(By.id(stopButtonId));
            }
            clickElement(button);
        } catch ( NotFoundException | NullPointerException e) {
            logger.error(e);
        }
    }
    public int checkProgressBarAndClickStopButton(int percent) {
        try {
            WebElement progressBar = findElement(By.id(progressBarId));
            while(true) {
                if(progressBar.getText().contains(String.valueOf(percent))){
                    clickButton("Stop");
                    break;
                }
            }
            WebElement result = findElement(By.id(resultId));
            String[] words = result.getText().split(",");
            String[] resultNum = words[0].split("\\s");
            return Integer.parseInt(resultNum[1]);
        } catch ( NotFoundException | NullPointerException e) {
            logger.error(e);
        }
        return -1;
    }
}
