Feature: API tests for petstore

  Scenario: Creating a pet in system
    When User wants to add a pet in the system
    Then User receive an id for pet

  Scenario: Order a pet in the system
    Given Pet defined to system
    When User wants to create an order for pet
    Then Order should be contain necessary info

  Scenario: User creation for pet store
    Given User created for pet store
    When User wanted to delete in the system
    Then System should add this user to the system