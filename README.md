# SpriteCloudAssignment

## How to run the tests locally
In your local system you can run the test by usıng TestRunner but 
there will be no browser pops up because code is using without header Chrome browser option.
## How to run the test in a CI/CD pipeline
With gitlab-ci.yaml file each commit will trigger the pipeline. Build, test and deploy by using maven package manager.
## Has a link to the results in Calliope.pro
https://app.calliope.pro/profiles/4342/reports
## Description one improvement point and one new feature for the Calliope.pro platform
I can be useful for users to have error message regarding wrong curl attempts. You have alraady warnings created for import option for the curl so that can be helpful for improvement.
## What I used to select the scenarios, what was your approach?
In UI tests, I try to create one smoke test for the page and 2 selected scenarios which is mentioned in the website. 
I created only happy path scenarios for UI.

For Api Tests, I try to use every http methods and try to assert the results with related get method also. (I put only one example for that)
## Why are they the most important
Smoke test is important because this kind of tests show us that system is working after new version. 
Happy path is the base of the system after getting happy paths you can get 
## What could be the next steps to my project
Negative tests and edge cases should be added to the scenarıos
